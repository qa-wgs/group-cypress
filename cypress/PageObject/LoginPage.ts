export class login{
    OpenBrowser(action: any) {
        cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login').then(function(){
            action()
        });
    }
    InputUsername() {
        cy.get('[name="username"]').type('Admin');
    }
    InputPassword() {
        cy.get('[name="password"]').type('admin123');
    }
    ClickBtn() {
        cy.get('button[type="submit"]').click();
    }
    SuccessLogin() {
        cy.get('.oxd-topbar-header-breadcrumb > .oxd-text').should('have.text','Dashboard');
    }
    UserLogin() {
        this.OpenBrowser
        this.InputUsername
        this.InputPassword
        this.ClickBtn
        this.SuccessLogin
    }
}