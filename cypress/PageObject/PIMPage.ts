export class PIMPage {
    moveToPIMpPage(action:any) {
        cy.visit("https://opensource-demo.orangehrmlive.com/web/index.php/pim/addEmployee").then(function(){
            action()
        })
    }
    inputForm(firstName: string, middleName: string, lastName: string) {
        cy.get('input[name="firstName"]').type(firstName);
        cy.get('input[name="middleName"]').type(middleName);
        cy.get('input[name="lastName"]').type(lastName);
    }
    clickSaveBtn() {
        cy.get('button[type="submit"]').click()
    }
    successAddPIM() {
        cy.get('.--visited > .oxd-topbar-body-nav-tab-item').should('have.text','Add Employee')
    }
}
