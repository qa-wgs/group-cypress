/// <reference types="Cypress" />
import { login } from "../PageObject/LoginPage"
import { PIMPage } from "../PageObject/PIMPage"

describe('editUser', () => {
    const Login = new login
    const pimPage = new PIMPage
    
    it('add user', () => {
        Login.OpenBrowser(function(){
            Login.InputUsername();
            Login.InputPassword();
            Login.ClickBtn();
            Login.SuccessLogin();

            pimPage.moveToPIMpPage(function(){
                pimPage.inputForm("jhon","test","case");
                pimPage.clickSaveBtn();
                pimPage.successAddPIM();
            })
        });
    })
});