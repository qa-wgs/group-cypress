const marge = require('mochawesome-report-generator');
const { merge } = require('mochawesome-merge');

merge({
  files: ['./cypress/reports/*.json'],
})
  .then((report) => marge.create(report))
  .then(() => {
    console.log('Report generated!');
  });